//
// Created by Irek Blicharski
//
#ifndef AHE_EXTENDEDCOMPLEX
#define AHE_EXTENDEDCOMPLEX

//#include <cmath>
#include "ComplexBase.hpp"

// PUNKT 4
// Napisać klasę extended_complex rozszerzającą funkcjonalność o zwracanie argumentu metoda abs() oraz modułu liczby
// zespolonej metoda arg(), oraz funkcji print() wypisującej liczbę na ekranie w postaci argumentu oraz modułu.
template<typename T>
class ExtendedComplex : public ComplexBase<T> {

public:
    explicit ExtendedComplex<T>(T Re=0.0, T Im=0.0) : ComplexBase<T>( Re, Im) { };

    // Returns the phase angle
    T abs() const;

    // Returns the magnitude
    T arg() const;

    virtual void print();
};

#endif //AHE_EXTENDEDCOMPLEX
