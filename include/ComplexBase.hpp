//
// Created by Irek Blicharski
//

#ifndef AHE_COMPLEX_BASE
#define AHE_COMPLEX_BASE

#include <iostream>

// PUNKT 1
// Zdefiniuj w C++ klasę reprezentującą liczbę zespoloną w postaci algebraicznej (ComplexBase), funkcja powinna
// posiadać konstruktor domyślny pozwalający na utworzenie obiektu klasy z dwóch zmiennych POD. Klasa powinna zawierać
// metodę print() wypisującą liczbę na ekranie, oraz zawierać metody real() imag() pozwalające na ustawienie oraz
// odczytanie osobno części rzeczywistej i urojonej.
//
// **[Dla chętnych]** Dodatkowym atutem będzie jeśli klasa będzie generyczna (z użyciem szablonów), oraz posiadać
// będzie funkcję zaprzyjaźnioną przeciążającą operator << umożliwiający wypisywanie  zawartości obiekt bezpośrednio
// do strumieni iostream
template<typename T>
class ComplexBase {
private:
    T _Re;
    T _Im;
public:
    explicit ComplexBase<T>(T Re=0.0, T Im=0.0) :_Re(Re), _Im(Im) { };
    ComplexBase(const ComplexBase &) = default;
//    ComplexBase<T>(const ComplexBase<T>& c) :_Re(c._Re), _Im(c._Im) { };

//    explicit ComplexBase(const T &__r = T(), const T &__i = T()) : _Re(__r), _Im(__i) {}

    virtual ~ComplexBase() {};

    template<typename U>
    friend std::ostream& operator <<(std::ostream&, const ComplexBase<U>&);

    virtual void print();
    T real() const;
    void real(T Re);
    T imag() const;
    void imag(T Im);
};
#endif //AHE_COMPLEX_BASE
