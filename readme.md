1. Zdefiniuj w C++ klasę reprezentującą liczbę zespoloną w postaci algebraicznej (complex_base), funkcja powinna posiadać konstruktor domyślny pozwalający na utworzenie obiektu klasy z dwóch zmiennych POD. Klasa powinna zawierać metodę print() wypisującą liczbę na ekranie, oraz zawierać metody real() imag() pozwalające na ustawienie oraz odczytanie osobno części rzeczywistej i urojonej. 

       **[Dla chętnych]** Dodatkowym atutem będzie jeśli klasa będzie generyczna (z użyciem szablonów), oraz posiadać będzie funkcję zaprzyjaźnioną przeciążającą operator << umożliwiający wypisywanie  zawartości obiekt bezpośrednio do strumieni iostream 

2. **[C++]** Proszę utworzyć instancję klasy complex_base o wartości X=7-j10 utworzyć stałą referencję Y do obiektu X a następnie wyświetlić zawartość Y na ekranie.

3. **[C++]** Używając inteligentnych wskaźników proszę utworzyć instancję klasy complex_base o wartości X=143-j24, a następnie utworzyć drugi inteligentny wskaźnik Y i przypisać mu obiekt X. Następnie należy wypisać na ekranie wartość Y

4. **[C++]** Napisać klasę extended_complex rozszerzającą funkcjonalność o zwracanie argumentu metoda abs() oraz modułu liczby zespolonej metoda arg(), oraz funkcji print() wypisującej liczbę na ekranie w postaci argumentu oraz modułu.

5. **[C++]** Proszę utworzyć instancję klasy extended_complex X=77-j5 używając alokacji dynamicznej, a następnie utworzyć stały typu complex_base i przypisać mu wskaźnik do poprzednio utworzonego obiektu. Wypisać zawartość liczby na ekranie, następnie obiekt należy zniszczyć.