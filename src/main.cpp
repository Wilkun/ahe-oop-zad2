//
// Created by Irek Blicharski
//

#include <iostream>
#include <iomanip>
#include <memory>

#include "ComplexBase.cpp"
#include "ExtendedComplex.cpp"

int main() {
    std::cout << std::fixed << std::setprecision(1);

    // PUNKT 2
    // Proszę utworzyć instancję klasy ComplexBase o wartości X=7-j10 utworzyć stałą referencję Y do obiektu X
    // a następnie wyświetlić zawartość Y na ekranie.
    {
        ComplexBase<int> X(7, -10);
        decltype(X) &Y = X;
        std::cout << "Punkt 2: Y = ";
        Y.print();
    }

    // PUNKT 3
    // Używając inteligentnych wskaźników proszę utworzyć instancję klasy ComplexBase o wartości X=143-j24,
    // a następnie utworzyć drugi inteligentny wskaźnik Y i przypisać mu obiekt X. Następnie należy wypisać
    // na ekranie wartość Y
    {
        auto X = std::make_shared<ComplexBase<int>> (143, -24);
        decltype(X) Y(X);
        std::cout << "Punkt 3: Y = ";
        Y->print();
    }

    // PUNKT 5
    // roszę utworzyć instancję klasy ExtendedComplex X=77-j5 używając alokacji dynamicznej, a następnie utworzyć
    // stały typu complex_base i przypisać mu wskaźnik do poprzednio utworzonego obiektu. Wypisać zawartość liczby
    // na ekranie, następnie obiekt należy zniszczyć.

    // Stały wskaźnik typu ComplexBase spowoduje że kod się nie skompiluje, ustawiam więc decltype()
    {
        auto * X = new ExtendedComplex<int>(77,-5);
        const decltype(X) &Y = X;
        std::cout << "Punkt 5: Y = ";
        Y->print();

        delete Y;
        delete X;
    }
}