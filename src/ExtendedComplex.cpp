//
// Created by Irek Blicharski
//
#include <iostream>
#include <cmath>
#include "ExtendedComplex.hpp"

template<typename T>
T ExtendedComplex<T>::arg() const {
    T __x = this->real();
    T __y = this->imag();
    const T __s = std::max(std::abs(__x), std::abs(__y));
    if (__s == T())
        return __s;
    __x /= __s;
    __y /= __s;
    return __s * sqrt(__x * __x + __y * __y);
}

template<typename T>
T ExtendedComplex<T>::abs() const {
    return  std::atan2( this->imag(), this->real());
}

template<typename T>
void ExtendedComplex<T>::print() {
    std::cout << "Modul: " << this->arg() << ", argument: " << this->abs() << std::endl;
}