//
// Created by Irek Blicharski
//

#include <iostream>
#include "ComplexBase.hpp"

template<typename T>
std::ostream& operator <<(std::ostream& os, const ComplexBase<T>& c) {
    os << c._Re;
    if (c._Im < 0)
        os << c._Im << "i";
    else if (c._Im > 0)
        os << "+" << c._Im << "i";
    return os;
}

template<typename T>
void ComplexBase<T>::print() {
    std::cout << *this << std::endl;
}

template<typename T>
T ComplexBase<T>::real() const {
    return _Re;
}

template<typename T>
void ComplexBase<T>::real(T Re) {
    _Re = Re;
}

template<typename T>
T ComplexBase<T>::imag() const {
    return _Im;
}

template<typename T>
void ComplexBase<T>::imag(T Im) {
    _Im = Im;
}